/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public class Triangulo extends FiguraGeometrica{

private double base;
private double altura;
private double profundidad;

    public Triangulo(double base, double altura,double profundidad) {
        this.base = base;
        this.altura = altura;
        this.profundidad = profundidad;
    }

public double area()
{
    return (base*altura)/2;
}

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

	@Override
	public double perimetro() {
		return 2*altura+base;
	}

	@Override
	public double volumen() {
		double S = perimetro()/2;
		
		double A = Math.sqrt(S*((S-base)*(2*(S-altura))));
		return A*profundidad;
	}
    
    
    
}
