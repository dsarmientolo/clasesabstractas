/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public class Circulo extends FiguraGeometrica{
    
    private double radio;

    public Circulo(double radio) {
        this.radio = radio;
    }
    
    public double area()
    {
        return Math.PI*Math.pow(radio, 2);
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

	@Override
	public double perimetro() {
		return 2*Math.PI*radio;
	}

	@Override
	public double volumen() {
		return (4/3)*Math.PI*Math.pow(radio, 3);
	}
    
    
}
