/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public class Rectangulo extends FiguraGeometrica{
    private double base;
    private double altura;
    private double profundidad;

    public Rectangulo(double base, double altura,double profundidad) {
        this.base = base;
        this.altura = altura;
        this.profundidad = profundidad;
    }

public double area()
{
    return base*altura;
}

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
    
	public double getProfundidad() {
		return profundidad;
	}

	public void setProfundidad(double profundidad) {
		this.profundidad = profundidad;
	}

	@Override
	public double perimetro() {
		return 2*(base+altura);
	}

	@Override
	public double volumen() {
		return base*altura*profundidad;
	}

}
